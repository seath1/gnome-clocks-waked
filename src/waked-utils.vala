/*
 * Copyright (C) 2021 Robin Westermann <waked@seath.de>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

namespace Clocks {
namespace WakedUtils {

    [DBus (name = "de.seath.Waked")]
    interface Waked : Object {
        public abstract void add (string id, uint64 time) throws GLib.Error;
        public abstract void update (string id, uint64 time) throws GLib.Error;
        public abstract void remove (string id) throws GLib.Error;
    }

    public void update_timer (string id, GLib.DateTime time) {
        try {
            Waked waked = Bus.get_proxy_sync (BusType.SYSTEM, "de.seath.Waked",
                                                              "/de/seath/Waked/Alarm");

            waked.update (id, time.to_unix());

        } catch (GLib.Error e) {
            stderr.printf ("%s\n", e.message);
        }
    }

    public void remove_timer (string id) {
        try {
            Waked waked = Bus.get_proxy_sync (BusType.SYSTEM, "de.seath.Waked",
                                                              "/de/seath/Waked/Alarm");

            waked.remove(id);

        } catch (GLib.Error e) {
            stderr.printf ("%s\n", e.message);
        }
    }
}
}
